# Serverless App / MongoDB Atlas : high number of connections

This example is a show case.

## Deploy your own

Deploy the example using [Vercel](https://vercel.com?utm_source=github&utm_medium=readme&utm_campaign=next-example):

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://github.com/ErnestBrandi/with-mongodb-test&project-name=with-mongodb-test&repository-name=with-mongodb-test)
