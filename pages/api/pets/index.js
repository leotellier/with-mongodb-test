import { connectToDatabase } from "../../../utils/dbConnect";

export default async function handler(req, res) {
  const { method } = req;
  const { db } = await connectToDatabase();

  switch (method) {
    case "GET":
      try {
        const cursor = await db.collection("pets").find({});
        const pets = await cursor.toArray();
        res.status(200).json({ success: true, pets });
      } catch (error) {
        console.log(error);
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const resp = await db.collection("pets").insertOne(req.body);
        if (!resp.insertedCount) throw { msg: "Insert fail" };
        res.status(201).json({ success: true });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
