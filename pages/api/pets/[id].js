import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../utils/dbConnect";

export default async function handler(req, res) {
  const {
    query: { id },
    method,
  } = req;

  const { db } = await connectToDatabase();

  switch (method) {
    case "GET":
      try {
        const pet = await db.collection("pets").findOne({ _id: ObjectId(id) });
        if (!pet) throw { msg: "Find fail" };
        res.status(200).json({ success: true, data: pet });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    case "PUT":
      try {
        const resp = await db
          .collection("pets")
          .findOneAndUpdate(
            { _id: ObjectId(id) },
            { $set: req.body },
            { returnOriginal: false }
          );
        res.status(200).json({ success: true, data: resp.value });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    case "DELETE" /* Delete a model by its ID */:
      try {
        const resp = await db
          .collection("pets")
          .deleteOne({ _id: ObjectId(id) });
        if (!resp.deletedCount) {
          throw { msg: "Delete fail" };
        }
        res.status(200).json({ success: true });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
}
